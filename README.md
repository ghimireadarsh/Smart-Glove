# Smart-Glove
Sign Language recognition system (model) developed using Random Forest Classifier, that translates the sign language alphabets and common words into text and sound.

* Supervised machine learning.

* System recognizes gestures through the use of flex sensor, accelerometer and the gyroscope.

* Coding is done in mac, so  adjust the port as per device port number.

* arduino_code.ino is burned inside the Arduino Mega2560.

* python_code whenever is executed it fetches the data from the serial port of the laptop then passes the data from the model for prediction of output as well as display and audio.

* Dataset is developed for the used system, so is not made public.

# Data Visualizations
<img src = "data_visualization.png">

## Correlation plots
* correlation plot of alphabet a <img src = "correlation_images/correlation_plot_alphabet_a.png">
* correlation plot of alphabet b <img src = "correlation_images/correlation_plot_alphabet_b.png">
* correlation plot of alphabet c <img src = "correlation_images/correlation_plot_alphabet_c.png">
* correlation plot of alphabet d <img src = "correlation_images/correlation_plot_alphabet_d.png">
* correlation plot of alphabet e <img src = "correlation_images/correlation_plot_alphabet_e.png">
* correlation plot of alphabet m <img src = "correlation_images/correlation_plot_alphabet_m.png">
* correlation plot of alphabet n <img src = "correlation_images/correlation_plot_alphabet_n.png">
* correlation plot of alphabet o <img src = "correlation_images/correlation_plot_alphabet_o.png">
* correlation plot of s <img src = "correlation_images/correlation_plot_alphabet_s.png">
* correlation plot of alphabet t <img src = "correlation_images/correlation_plot_alphabet_t.png">
* correlation plot of alphabet u <img src = "correlation_images/correlation_plot_alphabet_u.png">
* correlation plot of alphabet v <img src = "correlation_images/correlation_plot_alphabet_v.png">
